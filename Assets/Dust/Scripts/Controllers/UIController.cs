﻿using System;
using UnityEngine;

public class UIController : MonoBehaviour
{
    public static UIController instance { get; private set; }

    //=====Attributes=====
    [Header("Cameras")]
    [SerializeField] private Camera sceneCamera;
    [SerializeField] private Camera uiCamera;
    [Header("Panels")]
    [SerializeField] private UIContainerScreen containerScreen;
    [SerializeField] private UILoseScreen loseScreen;
    [SerializeField] private UIWinScreen winScreen;
    //********************

    #region Public Functions
    public void Init()
    {
        if (instance == null) instance = this;
    }

    public UIElement CreateUIElement(UIElement element)
    {
        if (element == null)
            return null;

        GameObject elementObject = NGUITools.AddChild(containerScreen.gameObject, element.gameObject);
        UIElement elementScript = elementObject.GetComponent<UIElement>();
        return elementScript;
    }

    public Vector3 FromSceneToUI(Vector3 position)
    {
        Vector3 result = sceneCamera.WorldToViewportPoint(position);
        result = uiCamera.ViewportToWorldPoint(result);

        return result;
    }

    public Vector3 FromUIToScene(Vector3 position)
    {
        Vector3 result = uiCamera.WorldToViewportPoint(position);
        result = sceneCamera.ViewportToWorldPoint(result);

        return result;
    }


    public void HideAll()
    {
        loseScreen.Hide();
        winScreen.Hide();
        containerScreen.Hide();
    }

    public void ShowWinScreen()
    {
        HideAll();
        winScreen.Show();
    }

    public void ShowLoseScreen()
    {
        HideAll();
        loseScreen.Show();
    }

    public void ShowContainer()
    {
        HideAll();
        containerScreen.Show();
    }
    #endregion
}
