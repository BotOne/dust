﻿using UnityEngine;

public class InputController : MonoBehaviour
{
    //=====Constants======
    private const float LONG_TAP_TIME  = 0.5f;
    private const float SWIPE_DISTANCE = 1.5f;
    //********************

    //=====Attributes=====
    [SerializeField] private Camera clickCamera;

    private InputDirection swipeDirection;
    private GameObject clickObject;
    private Vector3 clickPosition;
    private float clickDistance;
    private float clickTime;
    //********************

    //=======Flags========
    private bool isTap;
    //********************

    #region Events
    public delegate void OnTap(GameObject hoveredObject);
    public static event OnTap onTap;

    public delegate void OnLongTap(GameObject hoveredObject);
    public static event OnLongTap onLongTap;

    public delegate void OnSwipe(GameObject hoveredObject, InputDirection direction);
    public static event OnSwipe onSwipe;

    public delegate void OnTapRelease(GameObject hoveredObject, InputType type);
    public static event OnTapRelease onTapRelease;
    #endregion

    #region Private Functions
    private void UpdateInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            isTap         = true;
            clickTime     = 0;
            clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            clickObject   = GetHoveredObject();

            // Invoke tap event.
            if (onTap != null)
                onTap(clickObject);

            if (onTapRelease != null)
                onTapRelease(clickObject, InputType.Tap);
        }

        if (Input.GetMouseButton(0))
        {
            if (isTap)
            {
                // Check distance.
                clickDistance = Vector3.Distance(clickPosition, Camera.main.ScreenToWorldPoint(Input.mousePosition));
                if (clickDistance >= SWIPE_DISTANCE)
                {
                    isTap   = false;
                    swipeDirection = GetDirection();

                    // Invoke events.
                    if (onSwipe != null)
                        onSwipe(clickObject, swipeDirection);

                    if (onTapRelease != null)
                        onTapRelease(clickObject, InputType.Swipe);
                }

                // Check time.
                clickTime += Time.deltaTime;
                if (clickTime >= LONG_TAP_TIME)
                {
                    isTap     = false;

                    // Invoke events.
                    if (onLongTap != null)
                        onLongTap(clickObject);

                    if (onTapRelease != null)
                        onTapRelease(clickObject, InputType.LongTap);
                }
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            // Reset flags.
            isTap = false;

            // Reset object.
            clickObject = null;
        }
    }

    private InputDirection GetDirection()
    {
        Vector3 difference = clickPosition - Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (difference.y < 0 && difference.y < -SWIPE_DISTANCE) return InputDirection.Top;
        if (difference.y > 0 && difference.y > SWIPE_DISTANCE) return InputDirection.Down;
        if (difference.x > 0 && difference.x > SWIPE_DISTANCE) return InputDirection.Left;
        if (difference.x < 0 && difference.x < -SWIPE_DISTANCE) return InputDirection.Right;

        return InputDirection.None;
    }


    private GameObject GetHoveredObject()
    {
        RaycastHit[] hits = Physics.RaycastAll(clickCamera.ScreenPointToRay(Input.mousePosition));
        if (hits == null || hits.Length == 0) return null;

        return hits[0].transform.gameObject;
    }
    #endregion

    #region Unity Functions
    void Update ()
    {
        UpdateInput();
	}
    #endregion
}
