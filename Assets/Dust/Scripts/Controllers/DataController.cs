﻿#if UNITY_EDITOR
using UnityEditor;
#endif

using System;
using System.IO;
using System.Collections.Generic;
using System.Xml.Serialization;

using UnityEngine;

#region Tiles Data
[Serializable]
public class TileData
{
    public int ID;
    public string prefabName;
}

[XmlRoot("TilesData"), Serializable]
public class TileContainer
{
    public TileData GetTileByID(int ID)
    {
        if (tiles == null || tiles.Count == 0) return null;

        for (int i = 0; i < tiles.Count; ++i)
        {
            if (tiles[i].ID == ID) return tiles[i];
        }
        return null;
    }

    [XmlArray("Tiles"), XmlArrayItem("Tile")]
    public List<TileData> tiles;
}
#endregion

#region Units Data
[Serializable]
public class UnitData
{
    public string name;
    public UnitType type;
    public string prefabName;
    public string dieEffectPrefab;
    public int speed;
    public int life;
    public int attack;
    public int ID;
}

[XmlRoot("UnitsData"), Serializable]
public class UnitContainer
{
    public UnitData GetUnitByID(int ID)
    {
        if (units == null || units.Count == 0) return null;

        for (int i = 0; i < units.Count; ++i)
        {
            if (units[i].ID == ID) return units[i];
        }
        return null;
    }


    [XmlArray("Units"), XmlArrayItem("Unit")]
    public List<UnitData> units;
}
#endregion

#region Items Data
[Serializable]
public class ItemData
{
    public string name;
    public ItemType type;
    public string prefabName;
    public int ID;
}

[XmlRoot("ItemsData"), Serializable]
public class ItemContainer
{
    public ItemData GetItemByID(int ID)
    {
        if (items == null || items.Count == 0) return null;

        for (int i = 0; i < items.Count; ++i)
        {
            if (items[i].ID == ID) return items[i];
        }
        return null;
    }

    [XmlArray("Items"), XmlArrayItem("Item")]
    public List<ItemData> items;
}
#endregion

#region Maps Data
[Serializable]
public class MapData
{
    public int ID;
    public int size;
    public int playerID;
    public int exitID;
    public int bonusID;
    [XmlArray("tilesID"), XmlArrayItem("ID")]
    public List<int> tilesID;
    [XmlArray("enemiesID"), XmlArrayItem("ID")]
    public List<int> enemiesID;
    [XmlArray("itemsID"), XmlArrayItem("ID")]
    public List<int> itemsID;
    [XmlArray("map"), XmlArrayItem("tile")]
    public List<int> tiles;
}

[XmlRoot("MapsData"), Serializable]
public class MapContainer
{
    public MapData GetMapByID(int ID)
    {
        if (maps == null || maps.Count == 0) return null;

        for (int i = 0; i < maps.Count; ++i)
        {
            if (maps[i].ID == ID) return maps[i];
        }
        return null;
    }

    [XmlArray("Maps"), XmlArrayItem("Map")]
    public List<MapData> maps;
}
#endregion

#region Save Data
[Serializable]
public class SaveContainer
{
    //=====Attributes=====
    [SerializeField] private int _bestScore;

    public int bestScore
    {
        get
        {
            return _bestScore;
        }
        set
        {
            if (value > _bestScore)
            {
                _bestScore = value;
            }
        }
    }
    //********************
}
#endregion


public class DataController : MonoBehaviour
{
    public static DataController Instance { get; private set; }

    //=====Attributes=====
    public SaveContainer saveData;
    public TileContainer tilesData;
    public ItemContainer itemsData;
    public UnitContainer unitsData;
    public MapContainer mapsData;
    //********************

    #region Editor
#if UNITY_EDITOR
    [MenuItem("Data/Reset")]
    public static void ResetData()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
    }
#endif
#endregion

    #region Private Functions
    private void LoadTiles()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(TileContainer));
        TextAsset data = Resources.Load("Tiles") as TextAsset;
        using (var reader = new StringReader(data.text))
        {
            tilesData = serializer.Deserialize(reader) as TileContainer;
        }

        Debug.LogFormat("===== Load Tiles Complete. Loaded {0} tiles =====", tilesData.tiles.Count);
    }

    private void LoadUnits()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(UnitContainer));
        TextAsset data = Resources.Load("Units") as TextAsset;
        using (var reader = new StringReader(data.text))
        {
            unitsData = serializer.Deserialize(reader) as UnitContainer;
        }

        Debug.LogFormat("===== Load Units Complete. Loaded {0} units =====", unitsData.units.Count);
    }

    private void LoadItems()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(ItemContainer));
        TextAsset data = Resources.Load("Items") as TextAsset;
        using (var reader = new StringReader(data.text))
        {
            itemsData = serializer.Deserialize(reader) as ItemContainer;
        }

        Debug.LogFormat("===== Load Items Complete. Loaded {0} items =====", itemsData.items.Count);
    }

    private void LoadMaps()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(MapContainer));
        TextAsset data = Resources.Load("Maps") as TextAsset;
        using (var reader = new StringReader(data.text))
        {
            mapsData = serializer.Deserialize(reader) as MapContainer;
        }

        Debug.LogFormat("===== Load Maps Complete. Loaded {0} maps =====", mapsData.maps.Count);
    }
    #endregion

    #region Public Functions
    public void Init()
    {
        // Set simple singleton.
        if (Instance == null) Instance = this;

        // Loading configs.
        LoadMaps();
        LoadTiles();
        LoadUnits();
        LoadItems();

        // Loading save game.
        Load();
    }

    public void Load()
    {
        string dataString = PlayerPrefs.GetString("data", "");

        // Check for empty data.
        if (string.IsNullOrEmpty(dataString))
        {
            Save();
        }
        else
        {
            byte[] dataBytes = Convert.FromBase64String(dataString);
            saveData = (SaveContainer)Utils.ByteArrayToObject(dataBytes);
        }
    }

    public void Save()
    {
        byte[] dataBytes  = Utils.ObjectToByteArray(saveData);
        string dataString = Convert.ToBase64String(dataBytes);

        PlayerPrefs.SetString("data", dataString);
    }
    #endregion
}
