﻿using System;

using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class GameTrigger
{
    public string trigger;
    public UnityEvent action;
}

public class GameController : MonoBehaviour
{
    //=====Attributes=====
    [Header("Controllers")]
    [SerializeField] private UIController uiController;
    [SerializeField] private DataController dataController;
    [SerializeField] private InputController inputController;
    [SerializeField] private MapController mapController;
    [Header("Triggers")]
    [SerializeField] private GameTrigger[] triggers;
    [Header("Attributes")]
    [SerializeField] private int firstUnitID;
    [SerializeField] private int activeMapIndex;
    [SerializeField] private int activeUnitIndex;
    //********************

    #region Events
    public delegate void OnTriggerEvent(string trigger);
    public static event OnTriggerEvent onTrigger;
    public static void TriggerEvent(string trigger)
    {
        if (onTrigger != null)
            onTrigger(trigger);
    }
    #endregion

    #region Handlers
    private void OnTriggerHandler(string trigger)
    {
        for (int i = 0; i < triggers.Length; ++i)
        {
            if (triggers[i].trigger == trigger)
                triggers[i].action.Invoke();
        }
    }
    #endregion

    #region Private Functions
    private void AddListeners()
    {
        onTrigger += OnTriggerHandler;
    }

    private void RemoveListeners()
    {
        onTrigger -= OnTriggerHandler;
    }
    #endregion

    #region Public Functions
    public void NextTurn()
    {
        ++activeUnitIndex;
        if (activeUnitIndex >= mapController.unitsCount)
            activeUnitIndex = 0;

        // Start unit's turn.
        mapController.SetUnitTurn(activeUnitIndex);
    }


    public void ResetLevel()
    {
        mapController.ResetMap();
        GenerateLevel();
    }

    public void GenerateLevel()
    {
        MapData map = dataController.mapsData.GetMapByID(activeMapIndex);
        if (map != null)
            mapController.GenerateMap(map, StartLevel);

        // Show UI.
        uiController.ShowContainer();
    }

    public void StartLevel()
    {
        Debug.LogFormat("Level {0} started", activeMapIndex);

        // Get active unit index.
        activeUnitIndex = mapController.GetUnitIndexByID(firstUnitID);
        if (activeUnitIndex < 0)
            throw new Exception("ERROR: Player not found on map.");

        // Set turn to the active unit.
        mapController.SetUnitTurn(activeUnitIndex);
    }

    public void LoseLevel()
    {
        // Show UI.
        uiController.ShowLoseScreen();
    }

    public void CompleteLevel()
    {
        // Show UI.
        uiController.ShowWinScreen();
    }
    #endregion

    #region Unity Functions
    void Awake()
    {
        // Initialize all configs.
        uiController.Init();
        dataController.Init();
        mapController.Init();
    }

    void Start()
    {
        // Initialize active map.
        GenerateLevel();
    }

    void OnEnable()
    {
        AddListeners();
    }

    void OnDisable()
    {
        RemoveListeners();
    }
    #endregion
}
