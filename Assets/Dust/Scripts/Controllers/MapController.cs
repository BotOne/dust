﻿using System;

using UnityEngine;
using UnityEngine.Events;

public class MapController : MonoBehaviour
{
    //=====Attributes=====
    public Map activeMap;
    public int tilesCount
    {
        get
        {
            return activeMap.tilesCount;
        }
    }
    public int unitsCount
    {
        get
        {
            return activeMap.unitsCount;
        }
    }
    public int itemsCount
    {
        get
        {
            return activeMap.itemsCount;
        }
    }
    //********************

    #region Public Functions
    public void Init()
    {
        activeMap.Init();        
    }

    public void GenerateMap(MapData data, UnityAction callback)
    {
        activeMap.GenerateMap(data, callback);
    }

    public void ResetMap()
    {
        if (activeMap == null)
            throw new Exception("ERROR: active map not initialize.");

        activeMap.ResetMap();
    }

    public void UpdateMap()
    {
        if (activeMap == null)
            throw new Exception("ERROR: active map not initialize.");

        activeMap.UpdateMap();
    }


    public void SetUnitTurn(int unitIndex)
    {
        if (activeMap == null)
            throw new Exception("ERROR: active map not initialize.");

        activeMap.SetUnitTurn(unitIndex);
    }


    public Tile GetTileByPosition(MapPosition position)
    {
        return GetTileByPosition(position.x, position.y);
    }

    public Tile GetTileByPosition(int x, int y)
    {
        if (activeMap == null)
            throw new Exception("ERROR: active map not initialize.");

        return activeMap.GetTileByPosition(x, y);
    }

    public Tile GetTileByIndex(int index)
    {
        if (activeMap == null)
            throw new Exception("ERROR: active map not initialize.");

        return activeMap.GetTileByIndex(index);
    }

    public Unit GetUnitByIndex(int index)
    {
        if (activeMap == null)
            throw new Exception("ERROR: active map not initialize.");

        return activeMap.GetUnitByIndex(index);
    }

    public Unit GetUnitByID(int ID)
    {
        if (activeMap == null)
            throw new Exception("ERROR: active map not initialize.");

        return activeMap.GetUnitByID(ID);
    }

    public int GetUnitIndexByID(int ID)
    {
        if (activeMap == null)
            throw new Exception("ERROR: active map not initialize.");

        return activeMap.GetUnitIndexByID(ID);
    }

    public Item GetItemByIndex(int index)
    {
        if (activeMap == null) return null;

        return activeMap.GetItemByIndex(index);
    }
    #endregion
}
