﻿using UnityEngine;

public class Entity : MonoBehaviour
{
    //=======Cache========
    protected Transform mTransform;
    //********************

    //=====Attributes=====
    public Tile activeTile { get; private set; }
    //********************

    #region Virtual Functions
    public virtual void SetTile(Tile tile)
    {
        // Remove last tile.
        activeTile = tile;
        if (activeTile != null)
            mTransform.position = activeTile.worldPosition;
    }
    #endregion

    #region Unity Functions
    protected virtual void Awake()
    {
        mTransform = GetComponent<Transform>();
    }
    #endregion
}
