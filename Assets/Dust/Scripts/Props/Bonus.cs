﻿using UnityEngine;

public class Bonus : Item
{
    //=====Attributes=====
    public BonusType bonusType { get { return _bonusType; } }
    public int value { get { return _value; } }

    [SerializeField] private BonusType _bonusType;
    [SerializeField] private int _value;
    //********************
}
