﻿using UnityEngine;

public class Effect : MonoBehaviour
{
    #region Public Functions
    public void Dispose()
    {
        Destroy(gameObject);
    }
    #endregion
}
