﻿using UnityEngine;

public class Item : Entity
{
    //=====Attributes=====
    public ItemType type
    {
        get
        {
            return itemData.type;
        }
    }

    private ItemData itemData;
    //********************

    #region Virtual Functions
    public virtual void Dispose()
    {
        activeTile.SetItem(null);
        Destroy(gameObject);
    }
    #endregion

    #region Override Functions
    public override void SetTile(Tile tile)
    {
        base.SetTile(tile);

        if (tile != null)
            tile.SetItem(this);
    }
    #endregion

    #region Public Functions
    public void Init(ItemData itemData)
    {
        this.itemData = itemData;
    }
    #endregion
}
