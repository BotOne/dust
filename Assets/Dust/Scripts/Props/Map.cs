﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class MapPosition
{
    public MapPosition(MapPosition position)
    {
        x = position.x;
        y = position.y;
    }
    public MapPosition(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public static float Distance(MapPosition position0, MapPosition position1)
    {
        MapPosition position = new MapPosition(position0.x - position1.x, position0.y - position1.y);
        return Mathf.Sqrt(position.x * position.x + position.y * position.y);
    }

    public static MapPosition operator +(MapPosition c1, MapPosition c2)
    {
        return new MapPosition(c1.x + c2.x, c1.y + c2.y);
    }

    public int x;
    public int y;
}

public class Map : MonoBehaviour
{
    //=======Cache========
    private Transform mTransform;
    //********************

    //=====Attributes=====
    [SerializeField] private Vector2 tileSize;

    public int tilesCount
    {
        get
        {
            if (tiles == null) return -1;
            return tiles.Count;
        }
    }
    public int unitsCount
    {
        get
        {
            if (units == null) return -1;
            return units.Count;
        }
    }
    public int itemsCount
    {
        get
        {
            if (items == null) return -1;
            return items.Count;
        }
    }

    private List<Tile> tiles = new List<Tile>();
    private List<Unit> units = new List<Unit>();
    private List<Item> items = new List<Item>();
    private MapData data;
    //********************

    #region Private Functions
    private Tile GenerateTile(List<int> tilesID, MapPosition mapPosition, Vector2 worldPosition)
    {
        int index = UnityEngine.Random.Range(0, tilesID.Count);
        int ID = tilesID[index];

        // Get tile data.
        TileData data = DataController.Instance.tilesData.GetTileByID(ID);
        if (data == null)
            throw new Exception(String.Format("ERROR: Data for tile '{0}' not found.", ID));

        // Load tile from resources(or from unity's cache).
        Tile tilePrefab = ((GameObject)Resources.Load("Tiles/" + data.prefabName)).GetComponent<Tile>();
        if(tilePrefab == null)
            throw new Exception(String.Format("ERROR: Prefab for tile '{0}' not found.", data.prefabName));

        // Create tile.
        Tile tile = Instantiate(tilePrefab, mTransform);
        tile.SetWorldPosition(worldPosition);
        tile.SetMapPosition(mapPosition);

        // Add tile.
        tiles.Add(tile);

        return tile;
    }


    private Item GenerateItem(List<int> itemsID, Tile tile)
    {
        int index = UnityEngine.Random.Range(0, itemsID.Count);
        int ID = itemsID[index];

        return GenerateItem(ID, tile);
    }

    private Item GenerateItem(int itemID, Tile tile)
    {
        // Get item data.
        ItemData data = DataController.Instance.itemsData.GetItemByID(itemID);
        if (data == null)
            throw new Exception(String.Format("ERROR: Data for item '{0}' not found.", itemID));

        // Load item from resources(or from unity's cache).
        Item itemPrefab = ((GameObject)Resources.Load("Items/" + data.prefabName)).GetComponent<Item>();
        if (itemPrefab == null)
            throw new Exception(String.Format("ERROR: Prefab for item '{0}' not found.", data.prefabName));

        // Create item.
        Item item = Instantiate(itemPrefab, mTransform);

        // Link item and tile.
        item.Init(data);
        item.SetTile(tile);
        tile.SetItem(item);

        // Add item to the list.
        items.Add(item);

        return item;
    }


    private Unit GenerateUnit(List<int> unitsID, Tile tile)
    {
        int index = UnityEngine.Random.Range(0, unitsID.Count);
        int ID = unitsID[index];

        return GenerateUnit(ID, tile);
    }

    private Unit GenerateUnit(int unitID, Tile tile)
    {
        // Get unit data.
        UnitData data = DataController.Instance.unitsData.GetUnitByID(unitID);
        if (data == null)
            throw new Exception(String.Format("ERROR: Data for unit '{0}' not found.", unitID));

        // Load unit from resources(or from unity's cache).
        Unit unitPrefab = ((GameObject)Resources.Load("Units/" + data.prefabName)).GetComponent<Unit>();
        if (unitPrefab == null)
            throw new Exception(String.Format("ERROR: Prefab for unit '{0}' not found.", data.prefabName));

        // Create unit.
        Unit unit = Instantiate(unitPrefab, mTransform);

        // Link item and tile.
        unit.SetTile(tile);
        unit.Init(data, this);
        tile.SetUnit(unit);

        // Add unit.
        units.Add(unit);

        return unit;
    }
    #endregion

    #region Public Functions
    public void Init()
    {
        mTransform = GetComponent<Transform>();
    }


    public void SetUnitTurn(int unitIndex)
    {
        if (unitIndex < 0 || unitIndex >= unitsCount)
            throw new Exception(String.Format("ERROR: unit with index {0} not exits.", unitIndex));

        for (int i = 0; i < units.Count; ++i)
        {
            units[i].SetTurn(false);
        }

        units[unitIndex].SetTurn(true);
    }
    

    public Unit GetPlayerUnit()
    {
        return GetUnitByID(data.playerID);
    }


    public Tile GetTileByPosition(MapPosition position)
    {
        return GetTileByPosition(position.x, position.y);
    }

    public Tile GetTileByPosition(int x, int y)
    {
        for (int i = 0; i < tiles.Count; ++i)
        {
            if (tiles[i].mapPosition.x == x && tiles[i].mapPosition.y == y)
                return tiles[i];
        }
        return null;
    }

    public Tile GetTileByIndex(int index)
    {
        if (index < 0 || index >= tiles.Count)
            throw new Exception(String.Format("ERROR: tile with index {0} not exits.", index));

        return tiles[index];
    }


    public Unit GetUnitByIndex(int index)
    {
        if (index < 0 || index >= units.Count)
            throw new Exception(String.Format("ERROR: unit with index {0} not exits.", index));

        return units[index];
    }

    public Unit GetUnitByID(int ID)
    {
        for (int i = 0; i < units.Count; ++i)
        {
            if (units[i].ID == ID)
                return units[i];
        }
        return null;
    }

    public int GetUnitIndexByID(int ID)
    {
        for (int i = 0; i < units.Count; ++i)
        {
            if (units[i].ID == ID)
                return i;
        }
        return -1;
    }


    public Item GetItemByIndex(int index)
    {
        if (index < 0 || index >= items.Count)
            throw new Exception(String.Format("ERROR: item with index {0} not exits.", index));

        return items[index];
    }


    public void ResetMap()
    {
        // Remove map's data.
        for (int i = 0; i < tiles.Count; ++i)
        {
            if (tiles[i] != null)
                tiles[i].Dispose();
        }

        tiles.Clear();
        units.Clear();
        items.Clear();        
    }

    public void UpdateMap()
    {
        // Check units.
        for (int i = 0; i < units.Count; ++i)
        {
            if (units[i].isDead || units[i] == null)
            {
                units.RemoveAt(i);
            }
        }
    }

    public void GenerateMap(MapData data, UnityAction callback)
    {
        this.data = data;
        StartCoroutine(GenerateMapCoroutine(data, callback));
    }

    public IEnumerator GenerateMapCoroutine(MapData data, UnityAction callback)
    {
        // Check for the map's size.
        int size = data.size * data.size;
        if (size != data.tiles.Count)
            throw new Exception(String.Format("ERROR: Size set not correct({0}/{1})", size, data.tiles.Count));

        // Generate world position.
        Vector2 startPosition = Vector2.zero;
        startPosition.x = -(tileSize.x * (data.size - 1) / 2);
        startPosition.y = (tileSize.y * (data.size - 1) / 2);
        Vector2 worldPosition = startPosition;

        // Generate map and objects/units.
        for (int y = 0, index = 0; y < data.size; ++y)
        {
            for (int x = 0; x < data.size; ++x, ++index)
            {
                // Always generate tile.
                Tile tile = GenerateTile(data.tilesID, new MapPosition(x, y), worldPosition);

                switch (data.tiles[index])
                {
                    // Player tile.
                    case 1:
                        GenerateUnit(data.playerID, tile);
                        break;
                    // Enemy tile.
                    case 2:
                        GenerateUnit(data.enemiesID, tile);
                        break;
                    // Obstacle tile.
                    case 3:
                        GenerateItem(data.itemsID, tile);
                        break;
                    // Exit tile.
                    case 4:
                        GenerateItem(data.exitID, tile);
                        break;
                    case 5:
                        GenerateItem(data.bonusID, tile);
                        break;
                }

                worldPosition.x += tileSize.x;
                yield return new WaitForSeconds(0.005f);
            }
            worldPosition.x  = startPosition.x;
            worldPosition.y -= tileSize.y;
        }

        // Invoke callback function.
        if (callback != null)
            callback.Invoke();
    }
    #endregion
}
