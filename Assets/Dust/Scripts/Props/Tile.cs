﻿using UnityEngine;

public class Tile : MonoBehaviour
{
    //=======Cache========
    private Transform mTransform;
    //********************

    //=====Attributes=====
    public Item activeItem { get; private set; }
    public Unit activeUnit { get; private set; }
    public Vector2 worldPosition { get; private set; }
    public MapPosition mapPosition { get; private set; }
    //********************

    #region Public Functions
    public void Dispose()
    {
        if (activeItem != null)
            activeItem.Dispose();
        if (activeUnit != null)
            activeUnit.Dispose();
        Destroy(gameObject);
    }

    public void SetUnit(Unit unit)
    {
        activeUnit = unit;
    }

    public void SetItem(Item item)
    {
        activeItem = item;
    }

    public void SetWorldPosition(Vector2 position)
    {
        worldPosition = position;
        mTransform.position = worldPosition;
    }

    public void SetMapPosition(MapPosition position)
    {
        mapPosition = position;
    }
    #endregion

    #region Unity Functions
    void Awake()
    {
        mTransform = GetComponent<Transform>();
    }

    void OnDrawGizmos()
    {
        // Check unit.
        if (activeUnit != null)
            Gizmos.color = Color.green;
        else
            Gizmos.color = Color.gray;
        Vector3 positon = transform.position;
        positon.y -= 0.25f;

        Gizmos.DrawSphere(positon, 0.1f);

        // Check item
        if (activeItem != null)
            Gizmos.color = Color.red;
        else
            Gizmos.color = Color.gray;
        positon = transform.position;
        positon.y += 0.25f;

        Gizmos.DrawSphere(positon, 0.1f);
    }
    #endregion
}
