﻿using UnityEngine;

public class UIElement : MonoBehaviour
{
    //=======Cache========
    protected Transform mTransform;
    //********************

    //=======Flags========
    protected bool isVisible;
    //********************

    #region Virtual Functions
    public virtual void Init()
    {
        mTransform = GetComponent<Transform>();
    }


    public virtual void ApplyVisibility()
    {
        gameObject.SetActive(isVisible);
    }

    public virtual void Show()
    {
        isVisible = true;
        ApplyVisibility();
    }

    public virtual void Hide()
    {
        isVisible = false;
        ApplyVisibility();
    }

    public virtual void Switch()
    {
        isVisible = !isVisible;
        ApplyVisibility();
    }
    #endregion

    #region Unity Functions
    protected virtual void Awake()
    {
        Init();
    }
    #endregion
}
