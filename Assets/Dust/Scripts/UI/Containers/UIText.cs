﻿using UnityEngine;

public class UIText : UIElement
{
    //=====Attributes=====
    [SerializeField] private UILabel label;
    //********************

    #region Public Functions
    public void SetWorldPosition(Vector3 worldPosition)
    {
        Vector3 screenPosition = UIController.instance.FromSceneToUI(worldPosition);
        mTransform.position = screenPosition;
    }

    public void SetText(string text)
    {
        label.text = text;
    }

    public void SetColor(Color color)
    {
        label.color = color;
    }
    #endregion
}
