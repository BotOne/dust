﻿using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public enum InputDirection { None, Left, Right, Top, Down }

public enum InputType { None, Tap, LongTap, Swipe }

public enum ItemType { None, Target, Bonus }

public enum UnitType { None, Friend, Enemy }

public enum BonusType { None, Life }


public class Utils
{
    public static byte[] ObjectToByteArray(object obj)
    {
        if (obj == null)
            return null;
        BinaryFormatter binary = new BinaryFormatter();
        using (MemoryStream stream = new MemoryStream())
        {
            binary.Serialize(stream, obj);
            return stream.ToArray();
        }
    }

    public static object ByteArrayToObject(byte[] arrBytes)
    {
        using (var memStream = new MemoryStream())
        {
            var binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            var obj = binForm.Deserialize(memStream);
            return obj;
        }
    }
}
