﻿using UnityEngine;

public class Player : Unit
{
    #region Private Functions
    private void CollectBonus(Bonus bonus)
    {
        switch (bonus.bonusType)
        {
            case BonusType.None:
                break;
            case BonusType.Life:
                AdjustHealth(bonus.value);
                bonus.Dispose();
                UpdateUI();
                break;
        }
        GameController.TriggerEvent("unit_take_bonus");
        GameController.TriggerEvent("unit_complete_turn");
    }

    private void CompleteLevel()
    {
        GameController.TriggerEvent("unit_complete_level");
    }
    #endregion

    #region Handlers
    private void OnSwipeHandler(GameObject hoveredObject, InputDirection direction)
    {
        // Check flags.
        if (!onTurn)
            return;

        // Get next position.
        MapPosition nextPosition = new MapPosition(activeTile.mapPosition);
        switch (direction)
        {
            case InputDirection.Left:
                nextPosition.x -= unitData.speed;
                break;
            case InputDirection.Right:
                nextPosition.x += unitData.speed;
                break;
            case InputDirection.Top:
                nextPosition.y -= unitData.speed;
                break;
            case InputDirection.Down:
                nextPosition.y += unitData.speed;
                break;
        }

        // Get next tile.
        Tile nextTile = mapData.GetTileByPosition(nextPosition);
        if (nextTile == null) return;

        // Check unit.
        if (nextTile.activeUnit != null)
        {
            switch (nextTile.activeUnit.type)
            {
                case UnitType.Friend:
                    break;
                case UnitType.Enemy:
                    Attack(nextTile.activeUnit);
                    return;
            }
        }

        // Check items.
        if (nextTile.activeItem != null)
        {
            switch (nextTile.activeItem.type)
            {
                case ItemType.None:
                    return;
                case ItemType.Target:
                    CompleteLevel();
                    return;
                case ItemType.Bonus:
                    CollectBonus((Bonus)nextTile.activeItem);
                    return;
            }
        }

        // Move to next tile.
        Move(nextTile);
    }
    #endregion

    #region Override Functions
    public override void Die()
    {
        base.Die();
        GameController.TriggerEvent("unit_lose_level");
    }


    protected override void AddListeners()
    {
        InputController.onSwipe += OnSwipeHandler;
    }

    protected override void RemoveListeners()
    {
        InputController.onSwipe -= OnSwipeHandler;
    }
    #endregion
}
