﻿using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class Unit : Entity
{
    //=====Constants======
    private const float MOVE_TIME = 0.40f;
    //********************

    //=======Cache========
    private Animator mAnimator;
    //********************

    //=====Attributes=====
    [Header("UI")]
    [SerializeField] private UIText uiLifePrefab;
    [SerializeField] private Vector3 uiLifeOffset;

    [SerializeField] UIText uiAttackPrefab;
    [SerializeField] private Vector3 uiAttackOffset;

    public UnitType type
    {
        get { return unitData.type; }
    }
    public int ID
    {
        get
        {
            return unitData.ID;
        }
    }
    public int health { get; private set; }

    protected UIText uiLifeContainer;
    protected UIText uiAttackContainer;
    protected UnitData unitData;
    protected Map mapData;
    //********************

    //=======Flags========
    public bool onTurn { get; private set; }
    public bool isMove { get; private set; }
    public bool isDead { get; private set; }
    //********************

    #region Virtual Functions
    protected virtual void AddListeners()
    {
    }

    protected virtual void RemoveListeners()
    {
    }


    public virtual void SetTurn(bool status)
    {
        onTurn = status;
    }


    public virtual void Attack(Unit target)
    {
        if (target == null) return;
        if (target.isDead) return;

        mAnimator.SetTrigger("Attack");
        target.Hit(unitData.attack);

        // Set trigger.
        GameController.TriggerEvent("unit_complete_turn");
    }

    public virtual void Hit(int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            health = 0;
            Die();
        }
        else
        {
            mAnimator.SetTrigger("Hurt");
        }
        UpdateUI();
    }

    public virtual void Move(Tile tile)
    {
        if (isMove) return;

        mAnimator.SetTrigger("Move");
        UpdateDirection(tile);
        UpdatePosition(tile);
    }

    public virtual void Die()
    {
        isDead = true;
        mAnimator.SetTrigger("Die");
    }
    #endregion

    #region Override Functions
    public override void SetTile(Tile tile)
    {
        if(activeTile != null)
            activeTile.SetUnit(null);

        base.SetTile(tile);

        if (tile != null)
            tile.SetUnit(this);
    }
    #endregion


    #region Private Functions
    protected void GenerateAttributes()
    {
        if (unitData == null)
            throw new Exception("Error: Data not initialize");

        health = unitData.life;
    }

    protected void GenerateDieEffect()
    {
        GameObject effectPrefab = (GameObject)Resources.Load("Effects/" + unitData.dieEffectPrefab);
        if (effectPrefab == null)
            return;

        Instantiate(effectPrefab, mTransform.position, mTransform.rotation); 
    }

    protected void GenerateUI()
    {
        uiLifeContainer = (UIText)UIController.instance.CreateUIElement(uiLifePrefab);
        uiAttackContainer = (UIText)UIController.instance.CreateUIElement(uiAttackPrefab);
    }

    protected void UpdateUI()
    {
        uiLifeContainer.SetText(health.ToString());
        uiLifeContainer.SetWorldPosition(mTransform.position + uiLifeOffset);

        uiAttackContainer.SetText(unitData.attack.ToString());
        uiAttackContainer.SetWorldPosition(mTransform.position + uiAttackOffset);
    }

    protected void UpdatePosition(Tile tile)
    {
        StopCoroutine(UpdatePositionCoroutine(tile));
        StartCoroutine(UpdatePositionCoroutine(tile));
    }

    protected void UpdateDirection(Tile tile)
    {
        Vector3 scale = mTransform.localScale;
        if (activeTile.mapPosition.x < tile.mapPosition.x)
            scale.x = 1;
        else
            scale.x = -1;

        mTransform.localScale = scale;
        return;
    }

    protected IEnumerator UpdatePositionCoroutine(Tile tile)
    {
        float timer = 0;
        float value = 0;
        isMove      = true;
        while (timer <= MOVE_TIME)
        {
            timer += Time.deltaTime;
            value = timer / MOVE_TIME;

            mTransform.position = Vector3.Lerp(activeTile.worldPosition, tile.worldPosition, value);
            UpdateUI();
            yield return null;
        }
        SetTile(tile);

        // Set flag.
        isMove = false;

        // Set trigger.
        GameController.TriggerEvent("unit_complete_turn");
    }
    #endregion

    #region Public Functions
    public void Init(UnitData unitData, Map mapData)
    {
        this.unitData = unitData;
        this.mapData = mapData;

        GenerateAttributes();
        GenerateUI();
        UpdateUI();
    }

    public void Dispose()
    {
        activeTile.SetUnit(null);
        GenerateDieEffect();
        Destroy(uiLifeContainer.gameObject);
        Destroy(uiAttackContainer.gameObject);
        Destroy(gameObject);
    }


    public void AdjustHealth(int value)
    {
        health += value;
        if (health <= 0)
            Die();
    }
    #endregion

    #region Unity Functions
    protected override void Awake()
    {
        mTransform = GetComponent<Transform>();
        mAnimator = GetComponent<Animator>();
    }

    protected void OnEnable()
    {
        AddListeners();
    }

    protected void OnDisable()
    {
        RemoveListeners();
    }
    #endregion
}
