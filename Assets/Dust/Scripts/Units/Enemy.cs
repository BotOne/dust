﻿using UnityEngine;

public class Enemy : Unit
{
    #region Override Functions
    public override void SetTurn(bool status)
    {
        base.SetTurn(status);
        if (onTurn)
            MakeTurn();
    }
    #endregion

    #region Private Functions
    private Tile GetTile(MapPosition[] mapPositions, MapPosition targetPosition)
    {
        float minDistance = Mathf.Infinity;
        Tile minTile      = null;
        for (int i = 0; i < mapPositions.Length; ++i)
        {
            float distance = MapPosition.Distance(mapPositions[i], targetPosition);
            if (distance < minDistance)
            {
                // Check tile.
                Tile tile = mapData.GetTileByPosition(mapPositions[i]);
                if (tile == null)
                    continue;

                // Check item.
                if(tile.activeItem != null && tile.activeItem.type == ItemType.None)
                    continue;

                // Check unit.
                if (tile.activeUnit != null && tile.activeUnit.type == UnitType.Enemy)
                    continue;

                // Set attributes.
                minDistance = distance;
                minTile = tile;
            }
        }
        return minTile;
    }

    private void MakeTurn()
    {
        if (isDead)
        {
            GameController.TriggerEvent("unit_complete_turn");
            return;
        }

        Unit playerUnit = mapData.GetPlayerUnit();
        Tile playerTile = playerUnit.activeTile;

        // Check all blocks.
        MapPosition position = new MapPosition(activeTile.mapPosition);
        int x = position.x;
        int y = position.y;

        // Create blocks for checking.
        MapPosition[] checkPositions = new MapPosition[]
        {
            new MapPosition(x - unitData.speed, y),
            new MapPosition(x, y - unitData.speed),
            new MapPosition(x + unitData.speed, y),
            new MapPosition(x, y + unitData.speed)
        };

        Tile nextTile = GetTile(checkPositions, playerTile.mapPosition);
        if (nextTile == null)
        {
            GameController.TriggerEvent("unit_complete_turn");
            return;
        }

        // Check unit.
        if (nextTile.activeUnit != null && nextTile.activeUnit.type == UnitType.Friend)
        {
            Attack(nextTile.activeUnit);
            //GameController.TriggerEvent("unit_complete_turn");
            return;
        }

        // Make move.
        Move(nextTile);            
    }
    #endregion
}
